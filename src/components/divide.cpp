// divide.cpp
#include "divide.h"

double divide(int a, int b) {
    if (b == 0) {
        // Handle division by zero
        return 0.0;
    }

    return static_cast<double>(a) / b;
}
