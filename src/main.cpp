// main.cpp
#include "add.h"
#include "divide.h"
#include <iostream>

int main() {
    int sum = add(3, 4);
    double quotient = divide(10, 2);

    std::cout << "Sum: " << sum << std::endl;
    std::cout << "Quotient: " << quotient << std::endl;

    return 0;
}
